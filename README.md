# Plugin Descarga vCard de Usuario para WordPress

Plugin para descargar información de nuestros usuarios de WordPress como vCard. El visitante puede añadir nuestro contacto directamente a su móvil.
Repositorio privado https://gitlab.com/diurvan/diu-user-vcard

![Imagen](https://gitlab.com/diurvan/plugin-descarga-vcard-de-usuario-para-wordpress/-/raw/main/Logo_Plugin_vCard_WordPress_Banner.png)

Contribuye con tus comentarios.

Visita mi web en https://diurvanconsultores.com
